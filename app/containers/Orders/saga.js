
import { takeEvery, call, put, select } from 'redux-saga/effects';
import { DEFAULT_ACTION,FETCH_ORDER_DATA } from './constants';
// Individual exports for testing
export default function* ordersSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(FETCH_ORDER_DATA, fetchOrderData);
}

export function* fetchOrderData() {}