/*
 *
 * Orders reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION,UPDATE_ORDER_DETAILS } from './constants';
import {OrderData} from '../MockData/orderData';

export const initialState = {
  orderData: OrderData,
  loading:true
};


/* eslint-disable default-case, no-param-reassign */
const ordersReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case UPDATE_ORDER_DETAILS:
        draft.orderData = action.data;
        draft.loading = false;
        break;
      case DEFAULT_ACTION:
        break;
    }
  });

export default ordersReducer;
