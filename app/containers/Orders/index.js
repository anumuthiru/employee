/**
 *
 * Orders
 *
 */

import React, { memo,useEffect,useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectOrders,{selectOrdereData} from './selectors';
import {fetchOrderData} from './actions';
import GridRenderer from '../../components/GridRenderer';
import { updateActiveSection } from '../App/actions';
import reducer from './reducer';
import saga from './saga';

export function Orders({orderData,updateActiveSection}) {
  useInjectReducer({ key: 'orders', reducer });
  useInjectSaga({ key: 'orders', saga });
  useEffect(() => {
    updateActiveSection('Orders');
  }, []);

  const columnData=['SaleOrderId','Maximum','Minimum'];

  return (<div>
    <GridRenderer
        data={orderData.OrderGridData}
        columns={columnData}
        gridHeight={
           'calc(50vh - 110px)'
        }
      />
    </div>);
}

Orders.propTypes = {
  dispatch: PropTypes.func.isRequired,
  orderData: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  orders: makeSelectOrders(),
  orderData: selectOrdereData(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    fetchOrderData: () => {
      dispatch(fetchOrderData());
    },
    updateActiveSection: name => dispatch(updateActiveSection(name)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Orders);
