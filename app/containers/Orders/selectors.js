import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the orders state domain
 */

const selectOrdersDomain = state => state.orders || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Orders
 */

const makeSelectOrders = () =>
  createSelector(
    selectOrdersDomain,
    substate => substate,
  );

  const selectOrdereData = () =>
  createSelector(
    selectOrdersDomain,
    substate => substate.orderData,
  );

export default makeSelectOrders;
export { selectOrdersDomain ,
  selectOrdereData
};
