/*
 *
 * Orders constants
 *
 */

export const DEFAULT_ACTION = 'app/Orders/DEFAULT_ACTION';
export const FETCH_ORDER_DATA='app/Orders/FETCH_ORDER_DATA';
export const UPDATE_ORDER_DETAILS =  'app/Orders/UPDATE_ORDER_DETAILS'