/*
 *
 * Orders actions
 *
 */

import { DEFAULT_ACTION,FETCH_ORDER_DATA ,UPDATE_ORDER_DETAILS} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
  export function fetchOrderData() {
    return {
      type: FETCH_ORDER_DATA,
    };
  }
  
  export function updateOrderDetails(data) {
    return {
      type: UPDATE_ORDER_DETAILS,
      data,
    };
}
