/*
 *
 * App reducer
 *
 */
import produce from 'immer';
import {
  UPDATE_USER_DATA,
  UPDATE_NAVBAR,
  UPDATE_ACTIVE_TAB,
} from './constants';
import {UserData} from '../MockData/userData';
import {EmployeeData} from '../MockData/EmployeeData';

export const initialState = {
  userData: UserData.LoginData,
  //employeeGridData: EmployeeData.EmployeeGridData,
  activeSection: 'Employee',
  navSections: [
    {
      name: 'Employee',
      route: '/Dashboard',
    },
    {
      name: 'Orders',
      route: '/Orders',
    }
  ],
  isAuthenticated: sessionStorage.isAuthenticated
    ? JSON.parse(sessionStorage.isAuthenticated)
    : false,
  showNavBar: true,
};

/* eslint-disable default-case, no-param-reassign */
const AppReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case UPDATE_USER_DATA:
        draft.userData = action.userData;
        draft.isAuthenticated = true;
        break;
      case UPDATE_NAVBAR:
        draft.showNavBar = action.flag;
        break;
      case UPDATE_ACTIVE_TAB:
        draft.activeSection = action.name;
        break;
    }
  });

export default AppReducer;
