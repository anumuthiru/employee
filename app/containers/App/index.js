/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React ,{memo,useState}from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import NavBar from 'components/NavBar';
import ProtectedRoute from 'components/ProtectedRoute';

import Dashboard from 'containers/Dashboard/Loadable';
import Login from 'containers/Login/Loadable';
import Orders from 'containers/Orders/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Header from 'components/Header';
import Footer from 'components/Footer';

import {
  SelectAuthenticationStatus,
  SelectActiveTab,
  SelectNavTabs,
  SelectShowNavBar,
} from './selectors';
import { updateActiveSection } from './actions';
import GlobalStyle from '../../global-styles';
import '@progress/kendo-theme-default/dist/all.css';
import messages from './messages';

const AppWrapper = styled.div`
  max-width: calc(768px + 16px * 2);
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  padding: 0 16px;
  flex-direction: column;
`;

export function App({
  isAuthenticated,
  showNavBar,
  updateActiveTab,
  navSections,
  activeSection,
}) {
  return (
    <AppWrapper>
      <Header />
      {showNavBar && (
        <NavBar
          isAuthenticated={isAuthenticated}
          navSections={navSections}
          activeSection={activeSection}
          updateActiveTab={updateActiveTab}
          messages={messages}
          boxShadow="0px 2px 16px 0px rgba(33, 43, 54, 0.1)"
          navItemMargin="0 1.3em"
        />
      )}
      <Switch>
      <Route
          exact
          path="/"
          component={Login}
          isAuthenticated={!isAuthenticated}
        />
        <ProtectedRoute path="/Dashboard" component={Dashboard} isAuthenticated={isAuthenticated} />
        <ProtectedRoute path="/Orders" component={Orders} isAuthenticated={isAuthenticated} />
        <ProtectedRoute path="" component={NotFoundPage} isAuthenticated={isAuthenticated} />
      </Switch>
      <Footer />
      <GlobalStyle />
    </AppWrapper>
  );
}
const mapStateToProps = createStructuredSelector({
  isAuthenticated: SelectAuthenticationStatus(),
  navSections: SelectNavTabs(),
  activeSection: SelectActiveTab(),
  showNavBar: SelectShowNavBar(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    updateActiveTab: name => dispatch(updateActiveSection(name)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(App);