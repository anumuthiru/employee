/*
 * ButtonSection Messages
 *
 * This contains all the text for the NavBar component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.global';

export default defineMessages({
  'Employee': {
    id: `${scope}.Employee`,
    defaultMessage: 'Employee',
  },
  'Orders': {
    id: `${scope}.Orders`,
    defaultMessage: 'Orders',
  }
});
