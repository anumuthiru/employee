import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectRouter = state => state.router;

const selectGlobal = state => state.global || initialState;

const makeSelectLocation = () =>
  createSelector(
    selectRouter,
    routerState => routerState.location,
  );

const SelectAuthenticationStatus = () => {
  return createSelector(
    selectGlobal,
    globalState => JSON.parse(globalState.isAuthenticated),
  );
};

const SelectNavTabs = () => {
  return createSelector(
    selectGlobal,
    globalState => globalState.navSections,
  );
};

const SelectActiveTab = () => {
  return createSelector(
    selectGlobal,
    globalState => globalState.activeSection,
  );
};

const SelectShowNavBar = () => {
  return createSelector(
    selectGlobal,
    globalState => globalState.showNavBar,
  );
};

export {
  makeSelectLocation,
  SelectAuthenticationStatus,
  SelectNavTabs,
  SelectActiveTab,
  SelectShowNavBar,
};
