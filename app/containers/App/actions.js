import {
  UPDATE_USER_DATA,
  UPDATE_NAVBAR,
  UPDATE_ACTIVE_TAB,
} from './constants';

export function updateUserDetails(userData) {
  return {
    type: UPDATE_USER_DATA,
    userData,
  };
}
export function updateNavbar(flag) {
  return {
    type: UPDATE_NAVBAR,
    flag,
  };
}

export function updateActiveSection(name) {
  return {
    type: UPDATE_ACTIVE_TAB,
    name,
  };
}
