/*
 *
 * Dashboard reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION,UPDATE_EMPLOYEE_DETAILS } from './constants';
import {EmployeeData} from '../MockData/EmployeeData';

export const initialState = {
  employeeData: EmployeeData,
  loading:true
};

/* eslint-disable default-case, no-param-reassign */
const dashboardReducer = (state = initialState, action) =>
  produce(state, ( draft ) => {
    switch (action.type) {
      case UPDATE_EMPLOYEE_DETAILS:
        draft.employeeData = action.data;
        draft.loading = false;
        break;
      case DEFAULT_ACTION:
        break;
    }
  });

export default dashboardReducer;
