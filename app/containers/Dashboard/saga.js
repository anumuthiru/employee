import { takeEvery, call, put, select } from 'redux-saga/effects';
import { DEFAULT_ACTION,FETCH_EMPLOYEE_DATA } from './constants';
// import {EmployeeData} from '../MockData/EmployeeData';
// import {updateEmployeeDetails} from './actions';
// Individual exports for testing
export default function* dashboardSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(FETCH_EMPLOYEE_DATA, fetchEmployeeData);
}

export function* fetchEmployeeData() {

  // try {
  //  // const UserData = JSON.parse(sessionStorage.getItem('userData'));
    
  //   const response = EmployeeData.EmployeeGridData;
  //   yield put(updateEmployeeDetails(response));
  // } catch (err) {
  //   // eslint-disable-next-line react-hooks/rules-of-hooks
  //   UseErrorHandler(err);
  // }
}


