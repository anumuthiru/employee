/*
 *
 * Dashboard constants
 *
 */

export const DEFAULT_ACTION = 'app/Dashboard/DEFAULT_ACTION';
export const FETCH_EMPLOYEE_DATA='app/Dashboard/FETCH_EMPLOYEE_DATA';
export const UPDATE_EMPLOYEE_DETAILS =  'app/Dashboard/UPDATE_EMPLOYEE_DETAILS'