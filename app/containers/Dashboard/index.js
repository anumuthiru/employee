/**
 *
 * Dashboard
 *
 */

import React, { memo,useState,useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectDashboard,{selectEmployeeData} from './selectors';
import {fetchEmployeeData} from './actions';
import GridRenderer from '../../components/GridRenderer';
import { updateActiveSection } from '../App/actions';
import reducer from './reducer';
import saga from './saga';


export function Dashboard({employeeData,updateActiveSection}) {
  useInjectReducer({ key: 'dashboard', reducer });
  useInjectSaga({ key: 'dashboard', saga });

  useEffect(() => {
    updateActiveSection('Employee');
  }, []);

  const columnData=['Employee','EmployeeType','Gender'];

  return (<div>
    <GridRenderer
        data={employeeData.EmployeeGridData}
        columns={columnData}
        gridHeight={
           'calc(50vh - 110px)'
        }
      />
    </div>);
}

Dashboard.propTypes = {
  dispatch: PropTypes.func.isRequired,
  employeeData: PropTypes.object.isRequired,
};

// const propsTopass = {
//   employeeData: props.employeeData,
// };

const mapStateToProps = createStructuredSelector({
  dashboard: makeSelectDashboard(),
  employeeData: selectEmployeeData(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    fetchEmployeeData: () => {
      dispatch(fetchEmployeeData());
    },
    updateActiveSection: name => dispatch(updateActiveSection(name)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Dashboard);
