/*
 *
 * Dashboard actions
 *
 */

import { DEFAULT_ACTION ,FETCH_EMPLOYEE_DATA,UPDATE_EMPLOYEE_DETAILS} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function fetchEmployeeData() {
  return {
    type: FETCH_EMPLOYEE_DATA,
  };
}

export function updateEmployeeDetails(data) {
  return {
    type: UPDATE_EMPLOYEE_DETAILS,
    data,
  };
}
