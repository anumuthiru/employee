/*
 *
 * LoginPage actions
 *
 */

import { DEFAULT_ACTION, HANDLE_LOGIN } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function handleLogin(userData) {
  return {
    type: HANDLE_LOGIN,
    userData
  };
}
