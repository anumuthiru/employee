/*
 * LoginPage Messages
 *
 * This contains all the text for the LoginPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Login';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the LoginPage container!',
  },
  loginbtn: {
    id: `${scope}.loginbtn`,
    defaultMessage: 'Login',
  },
  appName: {
    id: `${scope}.appName`,
    defaultMessage: 'Cash App Advisor',
  },
  orgName: {
    id: `${scope}.orgName`,
    defaultMessage: 'accenture',
  },
});
