// import { take, call, put, select } from 'redux-saga/effects';

// Individual exports for testing
import { put, takeEvery } from 'redux-saga/effects';

import { HANDLE_LOGIN } from './constants';
import { updateUserDetails } from '../App/actions';
import { push } from 'connected-react-router';
import {UserData} from '../MockData/userData';
export default function* loginSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(HANDLE_LOGIN, handleLogin);
}
function* handleLogin(userData) {
  // on click of login button do the below 
  try {
    let data= UserData.LoginData;
    let user=data.filter(x=>x.UserName==userData.userData.username && x.Password==userData.userData.password);
    if(user!=null && user.length>0){
      yield put(updateUserDetails({ isAuthenticated: true,userData:userData.userData}));
     // sessionStorage.setItem('isAuthenticated', true);
      yield put(push('/Dashboard'));
    }
  } catch (err) {
      alert(err);
  }

}
