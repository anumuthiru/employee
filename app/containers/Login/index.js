/**
 *
 * Login
 *
 */

import React,{memo, useState} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import Input from '../../components/Input';
import { FormattedMessage } from 'react-intl';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { handleLogin } from './actions';
import makeSelectLoginPage from './selectors';
import messages from './messages';
import Button from '../../components/Button';
import Div from '../../components/Div';
import reducer from './reducer';
import saga from './saga';

export function Login({handleLogin}) {
  useInjectReducer({ key: 'login', reducer });
  useInjectSaga({ key: 'login', saga });

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const floatFocus=(args) => {
    args.target.parentElement.classList.add('e-input-focus');
}
const floatBlur=(args)=> {
    args.target.parentElement.classList.remove('e-input-focus');
}

const onChangeUsername = (e) => {
  const username = e.target.value;
  setUsername(username);
};

const onChangePassword = (e) => {
  const password = e.target.value;
  setPassword(password);
};

  return (<div>
    <div className="row"> 
    <Input
                id="username"
                type="text"
                placeholder="User Name"
                value={username}
                onChange={onChangeUsername}
                onFocus={floatFocus} onBlur={floatBlur}
              />
              </div>
              <div className="row">
       <Input
                id="password"
                type="password"
                placeholder="Password"
                value={password}
                onChange={onChangePassword}
                onFocus={floatFocus} onBlur={floatBlur}
              /> 
              </div>
              <div className="row">
              <Button classes="purpleButton" onClick={()=>handleLogin({username,password})}>
              <FormattedMessage {...messages.loginbtn} />
          &nbsp;&nbsp;
          <i className="far fa-angle-right" />
            </Button>        
            </div>
</div>);
}

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
  handleLogin: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  login: makeSelectLoginPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    handleLogin: (userData) => {
      dispatch(handleLogin(userData))
    }
  };
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Login);