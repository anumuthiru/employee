/*
 *
 * LoginPage constants
 *
 */

export const DEFAULT_ACTION = 'app/Login/DEFAULT_ACTION';
export const HANDLE_LOGIN = 'app/Login/HANDLE_LOGIN';