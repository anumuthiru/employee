/*
 *
 * Login reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION ,HANDLE_LOGIN} from './constants';
import {UserData} from '../MockData/userData';

export const initialState = {userData:UserData.LoginData};

/* eslint-disable default-case, no-param-reassign */
const loginReducer = (state = initialState, action) =>
  produce(state,  (draft)  => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case HANDLE_LOGIN:
        draft.userData= action.userData;
        break;  
    }
  });

export default loginReducer;
