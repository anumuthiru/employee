/**
 *
 * ProtectedRouter
 *
 */

import React from 'react';
import { Route } from 'react-router-dom';
// import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import ErrorPage from '../ErrorPage/index';

function ProtectedRoute(props) {
  const message = 'You are not Logged in. Please log in and try again.';
  return props.isAuthenticated === true ? (
    <Route exact component={props.component} />
  ) : (
    <ErrorPage Message={message} />
  );
}

ProtectedRoute.propTypes = {
  component: PropTypes.func,
  isAuthenticated: PropTypes.bool,
};

export default ProtectedRoute;
