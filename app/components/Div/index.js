/**
 *
 * Div
 *
 */

// import React from 'react';
// import PropTypes from 'prop-types';
 import styled from 'styled-components';

 const Div = styled.div`
 ${props=>{
switch (props.classes)
 {
   case 'main':
     return {
       width:'97%',
       margin:'auto'
     };
     case 'navbar-container': {
      return {
        width: '100%',
        borderRadius: ' 0px',
        boxShadow: props.boxShadow,
        paddingLeft: '1rem',
        background: ' rgb(255, 255, 255)',
        margin: props.margin,

        '@media only screen and (max-height: 630px)': {
          margin: props.margin && '10px ',
        },
      };
    }
    case 'navBar':
        return {
          display: 'flex',
          flexWrap: 'nowrap',
          justifyContent: 'flex-start',
          background: ' rgb(255, 255, 255)',
          width: '100%',

          '@media only screen and (max-height: 800px)': {
            height: '40px',
          },
        };
      case 'navItem':
        return {
          display: 'flex',
          flexDirection: 'column',
          margin: props.navItemMargin,
          color: 'rgb(99, 115, 129)',
          fontFamily: ' Graphik-Medium-Web',
          fontSize: '14px',
          fontWeight: '500',
          letterSpacing: '-0.25px',
          padding: '12px 0px 8px 0px',

          '@media only screen and (max-height: 800px)': {
            fontSize: '13px',
          },
        };
      case 'usernavItem':
        return {
          display: 'none',
          marginLeft: '15px',
          color: 'rgb(99, 115, 129)',
          fontFamily: ' Graphik-Medium-Web',
          fontSize: '1rem',
          fontWeight: '500',
          letterSpacing: '-0.25px',
        };
      case 'activeNavItem':
        return {
          display: 'flex',
          flexDirection: 'column',
          margin: props.navItemMargin,
          color: ' rgb(161, 0, 255)',
          fontFamily: ' Graphik-Medium-Web',
          fontSize: '14px',
          fontWeight: '500',
          padding: '12px 0px 8px 0px',
          borderBottom: '4px solid rgb(161, 0, 255)',
          letterSpacing: '-0.25px',

          '@media only screen and (max-height:800px)': {
            fontSize: '13px',
          },
        };
 }
 }}
`;

export default Div;

