/**
 *
 * ErrorPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import Div from '../Div';

// import styled from 'styled-components';
 //import Alert from 'react-bootstrap/Alert';

function ErrorPage(props) {
   return ( <Div> {props.Message}</Div>);
}

ErrorPage.propTypes = {
Message: PropTypes.string,
};

export default ErrorPage;
