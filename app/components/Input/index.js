/**
 *
 * Select
 *
 */

// import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const Input = styled.input`
  ${props => {
    switch (props.classes) {
      
      case 'tag-input':
        return {
          padding: '23px 40px 6px 10px',
          color: 'rgb(123, 142, 152)',
          fontFamily: 'Graphik-Regular-Web',
          fontSize: '12px',
          fontWeight: 'bold',
          background: 'rgb(250, 250, 250)',
          borderRadius: '4px',
          border: '1px solid rgb(223, 227, 232)',
          // width:'70%',
        };
      case 'tag-input2':
        return {
          padding: '23px 5px 6px 10px',
          color: 'rgb(123, 142, 152)',
          fontFamily: 'Graphik-Regular-Web',
          fontSize: '12px',
          fontWeight: 'bold',
          background: 'rgb(250, 250, 250)',
          borderRadius: '4px',
          border: '1px solid rgb(223, 227, 232)',
          width: '100%',
        };
      case 'tag-input2-error':
        return {
          padding: '23px 5px 6px 10px',
          color: 'rgb(123, 142, 152)',
          fontFamily: 'Graphik-Regular-Web',
          fontSize: '12px',
          fontWeight: 'bold',
          background: 'rgb(250, 250, 250)',
          borderRadius: '4px',
          border: '1px solid rgb(214, 0, 47)',
          width: '100%',
        };
      case 'user-inputyp':
        return {
          paddingBottom: '0px !Important',
          height: '56px !Important',
          color: '#212b36 !important',
          fontWeight: 'normal !important',
        };

      case 'user-inputypdate':
        return {
          // paddingBottom:'0px !Important',
          height: '56px !Important',
          color: '#919EAA !important',
        };

      case 'user-inputypmail':
        return {
          paddingBottom: '0px !Important',
          height: '56px !Important',
          color: '#212b36 !important',
          fontWeight: 'normal !important',
          paddingLeft: '35px !Important',
        };

      case 'td_review_input1':
        return {
          background: 'rgb(250, 250, 250)',
          borderRadius: '4px',
          border: '1px solid rgb(223, 227, 232)',
          color: '#000',
          marginBottom: '10px',
          width: '25%',
          position: 'relative',
          '::placeholder': {
            paddingLeft: '10px',
          },
          ':hover': {
            border: '1px solid rgb(130, 220, 252)',
          },
        };

      case 'td_review_input2':
        return {
          background: 'rgb(250, 250, 250)',
          borderRadius: '4px',
          border: '1px solid rgb(223, 227, 232);',
          color: '#000',
          marginBottom: '10px',
          width: '25%',
          position: 'relative',
          '::placeholder': {
            paddingLeft: '10px',
          },
          ':hover': {
            border: '1px solid rgb(130, 220, 252)',
          },
        };

      case 'td_generic_input':
        return {
          background: 'rgb(250, 250, 250)',
          border: '1px solid rgb(223, 227, 232)',
          width: '100%',
          paddingLeft: '10px',
          color: '#2d3a4b',
          // marginLeft:'30px',
        };
      case 'reguest-labelinput':
        return {
          border: 'none',
          display: 'block',
          width: '100%',
          fontFamily: 'Graphik-Medium-Web',
          fontSize: '0.85em',
          color: '#000',
          /* padding: 5px; */
          background: 'right center no-repeat',
          paddingLeft: '3px',
        };
      case 'statement-input-checkbox':
        return {
          width: '20px',
          height: '18px',
        };
      case 'statement-date-format':
        return {
          padding: '8px 40px 6px 6px',
          background: 'rgb(250, 250, 250)',
          borderRadius: '4px',
          border: '1px solid rgb(223, 227, 232)',
          color: 'rgb(45, 58, 75)',
          fontFamily: 'Graphik-Regular-Web',
          fontSize: '14px',
          fontWeight: '500',
          textTransform: 'capitalize',
          //width: '125px !important',
          position: 'relative',
          marginBottom: '10px',
          marginRight: '10px',
          width: '100%',
        };

      case 'statement_input':
        return {
          border: '1px solid rgb(223, 227, 232)',
          borderRadius: '4px',
          marginRight: '10px',
          padding: '2px',
          color: '#2d3a4b',
          background: '#fafafa',
          marginLeft: '20px',
        };
      case 'activestatus-checkbox':
        return {
          width: '20px',
          height: '20px',
          position: 'relative',
          top: '5px',
          marginRight: '5px',
        };

      default:
        return {};
    }
  }};
`;

export default Input;
