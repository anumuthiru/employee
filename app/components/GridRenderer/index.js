/**
 *
 * GridRenderer
 *
 */

import React, { memo, useState } from 'react';
import { Grid, GridColumn } from '@progress/kendo-react-grid';
import '@progress/kendo-theme-default/dist/all.css';
import PropTypes from 'prop-types';
import Div from '../Div/index';
import GridCellRenderer from '../GridCellRenderer/index';
import { orderBy } from '@progress/kendo-data-query';

const rowRenderer = trElement => {
  const Style = {
    backgroundColor: ' rgb(255, 255, 255)',
  };
  const trProps = { style: Style };
  return React.cloneElement(
    trElement,
    { ...trProps },
    trElement.props.children,
  );
};

function GridRenderer({
  data,
  columns,
  gridHeight,
  handleRowSelection,
  updateSelectedRows,
  columnData,
  UpdateColumns,
  fullWidth,
}) {
  const [sort, setSort] = useState([]);
  const [skip, setSkip] = useState(0);
  const [take, setTake] = useState(5);
  const [show, setShow] = useState(false);

  const pageChange = event => {
    setSkip(event.page.skip);
    setTake(event.page.take);
  };
  const handleShow = () => {
    if (show === false) {
      setShow(true);
    } else {
      setShow(false);
    }
  };

  const handleRowClick = e => {};

  const handleSelectAll = e => {
    const checked = e.syntheticEvent.target.checked;
    var list = document.getElementsByClassName('k-checkbox');
    for (const el of list) {
      el.checked = checked;
    }
    const data = checked ? e.target.props.data : [];
    updateSelectedRows(data);
  };

  return (
    <Div classes="Grid">
      <Grid
        data={orderBy(data.slice(skip, take + skip), sort)}
        sortable
        sort={sort}
        selectable
        scrollable
        resizable={!fullWidth}
        skip={skip}
        take={take}
        pageable={{
          buttonCount: 5,
          info: true,
          type: 'numeric',
          pageSizes: [5, 10, 15],
          previousNext: true,
        }}
        onPageChange={pageChange}
        total={data.length}
        style={{ height: gridHeight }}
        rowRender={rowRenderer}
        selectedField="Selected"
        onSortChange={e => {
          setSort(e.sort);
        }}
        skip={skip}
        take={take}
        onPageChange={pageChange}
      >
        <GridColumn field="Selected" locked />
        {columns &&
          columns.length &&
          columns.map(each => {
            return  (
                  <GridColumn
                    field={each}
                    title={each}
                    cell={e => <GridCellRenderer e={e} />}
                    width={200}
                  />
                );
          })}
      </Grid>
    </Div>
  );
}

GridRenderer.propTypes = {
  data: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  gridHeight: PropTypes.string.isRequired
};

export default memo(GridRenderer);
