/**
 *
 * NavBar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Div from 'components/Div/index';
import withAuthentication from 'HOC/withAuthentication';
import history from '../../utils/history';
import { FormattedMessage } from 'react-intl';
// import messages from './messages';

function NavBar({
  activeSection,
  navSections,
  updateActiveTab,
  messages,
  boxShadow,
  margin,
  navItemMargin,
}) {
  const handleNavOnClick = section => {
    if (section.route) {
      history.push(section.route);
    }
    updateActiveTab(section.name);
  };

  if (!navSections || !navSections.length) return <></>;
  return (
    <Div classes="navbar-container" boxShadow={boxShadow} margin={margin}>
      <Div classes="navBar">
        {navSections.map((section, index) => (
          <Div
            classes={
              section.name === activeSection ? 'activeNavItem' : 'navItem'
            }
            key={index}
            onClick={() => handleNavOnClick(section)}
            navItemMargin={navItemMargin}
          >
            <FormattedMessage {...messages[`${section.name}`]} />
          </Div>
        ))}
      </Div>
    </Div>
  );
}

export default withAuthentication(NavBar);
