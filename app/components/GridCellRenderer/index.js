/**
 *
 * GridCellRenderer
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';

function GridCellRenderer({ e }) {
  return <td>{e.dataItem[e.field]}</td>;
}

GridCellRenderer.propTypes = {};

export default memo(GridCellRenderer);
