import React from 'react';
import LoadingIndicator from 'components/LoadingIndicator';

const withLoader = Component => props => {
  if (props.isLoading) return <LoadingIndicator />;
  return <Component {...props} />;
};

export default withLoader;
