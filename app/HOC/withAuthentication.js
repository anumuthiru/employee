import React from 'react';

const withAuthentication = Component => props => {
  if (!props.isAuthenticated) return <></>;
  return <Component {...props} />;
};

export default withAuthentication;
